import React, { Component } from 'react'
class ClickCounter extends Component {
  constructor(props) {
    console.log('enter constructor-'+ props.caption)
    super(props)
    this.onClickIncreButton = this.onClickIncreButton.bind(this)
    this.onClickDescreButton = this.onClickDescreButton.bind(this)

    this.state = {
      count: props.initValue || 0
    }
  }

  componentWillMount(){
    console.log('enter componentWillMount-'+this.props.caption)
  }

  componentWillReceiveProps(){
    console.log('enter componentWillReceiveProps-'+this.props.caption)
  }

  shouldComponentUpdate(nextProps,nextState){
    return (nextProps.caption !==this.props.caption) || (nextState.count !== this.state.count)
  }
  componentDidMount(){
    console.log('enter componentDidMount-'+this.props.caption)
  }
  componentWillUpdate(){
    console.log('enter componentWillUpdate-'+this.props.caption)
  }
  componentDidUpdate(){
    console.log('enter componentDidUpdate-'+this.props.caption)
  }
  componentWillUnmount(){
    console.log('enter componentWillUnmount-'+this.props.caption)
  }
  onClickIncreButton(){
    this.increOrDescreButton(true)
    // this.setState({
    //   count: this.state.count + 1
    // })
  }
  onClickDescreButton(){
    this.increOrDescreButton(false)
    // this.setState({
    //   count: this.state.count - 1
    // })
  }
  increOrDescreButton(isIncre) {
    const previousValue = this.state.count
    const newValue = isIncre ? previousValue + 1 :previousValue - 1;
    this.setState({
      count: newValue
    })
    this.props.onUpdate(newValue,previousValue)
  }

  render() {
    console.log('enter render-'+ this.props.caption)
    const { caption } = this.props
    return (
      <div>
        <button onClick={this.onClickIncreButton}> + </button>
        <button onClick={this.onClickDescreButton}> - </button>
        <span> {caption} count :{this.state.count} </span>
      </div>
    )
  }
}


// ClickCounter.propTypes ={
//   caption:PropTypes.string.isRequired,
//   initValue:PropTypes.number
// }

export default ClickCounter
