import React, { Component } from 'react'
import ClickCounter from './ClickCounter'

class ControlPanel extends Component {
  constructor(props){
    super(props)
    this.onCounterUpdate = this.onCounterUpdate.bind(this)

    this.initValues = [0 ,10 , 20]
    const initSum = this.initValues.reduce((a,b)=> a+b,0)
    this.state = {
      sum:initSum
    }
  }
  onCounterUpdate(newValue,previousValue){
    const valueChange = newValue - previousValue
    this.setState({sum:this.state.sum + valueChange})
  }
  render() {
    return (
      <div>
        <ClickCounter onUpdate = { this.onCounterUpdate } caption="first" initValue = {this.initValues[0]}/>
        <ClickCounter onUpdate = { this.onCounterUpdate } caption="second" initValue = {this.initValues[1]}/>
        <ClickCounter onUpdate = { this.onCounterUpdate } caption="third" initValue = {this.initValues[2]}/>
        <button onClick={()=>{ this.forceUpdate() }}>重绘</button>
        <div>total count: {this.state.sum}</div>
      </div>
    )
  }
}
export default ControlPanel
